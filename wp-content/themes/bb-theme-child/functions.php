<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );


// Classes
require_once 'classes/class-fl-child-theme.php';



//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("flooring finder",get_stylesheet_directory_uri()."/script.js","","",1);
  //  wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

 // Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
  //  write_log($sql_delete);	
}
add_filter( 'auto_update_plugin', '__return_false' );


//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


add_filter( 'facetwp_template_html', function( $output, $class ) {
    //$GLOBALS['wp_query'] = $class->query;
    $prod_list = $class->query;  
    ob_start();       
   // write_log($class->query_args['check_instock']);
    if(isset($class->query_args['check_instock']) && $class->query_args['check_instock'] == 1){

        $dir = get_stylesheet_directory().'/product-instock-loop.php';

    }else{

        $dir = get_stylesheet_directory().'/product-loop.php';
    }
   
    require_once $dir;    
    return ob_get_clean();
}, 10, 2 );




//check out flooring FUnctionality

add_action( 'wp_ajax_nopriv_check_out_flooring', 'check_out_flooring' );
add_action( 'wp_ajax_check_out_flooring', 'check_out_flooring' );

function check_out_flooring() {

    $arg = array();
    if($_POST['product']=='carpeting'){

        $posttype = array('carpeting');

    }elseif($_POST['product']=='hardwood_catalog,laminate_catalog,luxury_vinyl_tile'){

        $posttype = array('hardwood_catalog','laminate_catalog','luxury_vinyl_tile');

    }elseif($_POST['product']=='tile_catalog' || $_POST['product']=='tile'){

        // $posttype = array('tile_catalog','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','carpeting');
        $posttype = array('tile_catalog');
    }

    $parameters =  explode(",",$_POST['imp_thing']);

foreach($parameters as $para){
     if($para == 'hypoallergenic'){

        $arg['hypoallergenic'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50
        );
     }elseif($para == 'diy_friendly'){

        $arg['diy_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'installation_facet',
                    'value'   => 'Locking',
                    'compare' => '=',
                    
                ),
                array(
                    'key'     => 'installation_method',
                    'value'   => 'Locking',
                    'compare' => '=',
                    
                ),
            ),             
        );
     }elseif($para == 'aesthetic_beauty'){

        $arg['aesthetic_beauty'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'brand',
                    'value'   => 'Anderson Tuftex',
                    'compare' => '=',
                    
                ),
                array(
                    'key'     => 'brand',
                    'value'   => 'Karastan',
                    'compare' => '=',
                    
                ),
            ),
        );
     }elseif($para == 'eco_friendly'){
        $arg['eco_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Eco',
                    'compare' => 'LIKE',
                    
                ),
            ),
        );
     }
     elseif($para == 'pet_friendly'){

        $arg['pet_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Pet',
                    'compare' => 'LIKE',
                    
                ),
            ),
        );

        
     }elseif($para == 'easy_to_clean'){
        
        $arg['easy_to_clean']=array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }elseif($para == 'durability'){
        
        $arg['durability']=array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'Stainmaster',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }
     elseif($para == ''){
        
        $arg['colors']=array(
            'post_type'      => array( 'luxury_vinyl_tile', 'laminate_catalog','hardwood_catalog','carpeting','tile' ),
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
        );

        
     }
     elseif($para == 'stain_resistant'){
        
        $arg['stain_resistant'] =array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Stain Resistant',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'Stainmaster',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'Collection',
                    'value'   => 'Bellera',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }elseif($para == 'budget_friendly'){
        
        $arg['budget_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                array(
                    'key'     => 'fiber',
                    'value'   => array( 'SmartStrand','Stainmaster','Bellera','Pet R2x' ),
                    'compare' => 'IN',
                    
                ),
            ),
        );

        
     }elseif($para == 'color'){
        
        $arg['color'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50            
        );

        
     }

}
 
     $i = 1;

     $find_posts = array();
     foreach($parameters as $para){
        $wp_query[$i] = new WP_Query($arg[$para]);
        
        
        $find_posts = array_merge( $find_posts , $wp_query[$i]->posts);        

        $i++;
     }
     
     //print_r($find_posts);
     if (!$find_posts ) {
       
            $args =array(
                'post_type'      => $posttype,
                'post_status'    => 'publish',
                'posts_per_page' => 50,
                'orderby'        => 'rand'
            );
            $find_posts   = new WP_Query($args);
           // var_dump($args , $find_posts);
            $find_posts = $find_posts->posts;
     }

     if ( $find_posts ) {
          
        $content ="";

        foreach ( $find_posts as $post ) {

            $image = swatch_image_product_thumbnail($post->ID,'222','222');
            $sku = get_field( "sku", $post->ID );
           
     $content .= '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="'.$image.'" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>'.get_field( "collection", $post->ID ).'</h4> 
                                <h3>'.get_field( "color", $post->ID ).'</h3>
                                <p class="brand"><small><i>'.get_field( "brand", $post->ID ).'</i></small></p>
                                <!-- <a class="orderSamplebtn fl-button btnAddAction cart-action"  href="javascript:void(0)">
                                Order sample
                                </a> -->
                                <a class="prodLink" href="'.get_permalink($post->ID).'">See info</a>
                            </div>
                            
                        </div>
                    </div>';
        }

        $content .='<div class="text-center buttonWrap" data-search="'.$_POST['imp_thing'].'">
                    <a class="button" href="javascript:void(0)">view all results</a>
                    <a class="restartQuiz prodLink" href="javascript:void(0)" onclick="location.reload();">Restart Quiz</a>
                </div>';

    }

    echo $content;
    wp_die();
}

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_new' );

function wpse_100012_override_yoast_breadcrumb_trail_new( $links ) {
    global $post;
    $instock = get_post_meta( $post->ID , "in_stock");

    if ( is_singular( 'hardwood_catalog' )  ) {
        if($instock[0] == "1"){
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/in-stock-hardwood-products/',
                'text' => 'In Stock Hardwood Products',
            );
        }else{
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/hardwood/',
                'text' => 'About Hardwood',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/hardwood/products/',
                'text' => 'Hardwood Products',
            );
        }    
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'carpeting' )) {
        if($instock[0] == "1"){
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/in-stock-carpet-products/',
                'text' => 'In Stock Carpet Products',
            );
        }else{    
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/carpet/',
                'text' => 'About Carpet',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/carpet/products/',
                'text' => 'Carpet Products',
            );
        }    
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'luxury_vinyl_tile' )) {
        if($instock[0] == "1"){
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/in-stock-waterproof-luxury-vinyl-products/',
                'text' => 'In Stock Waterproof Luxury Vinyl Products',
            );
        }else{    
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/waterproof-luxury-vinyl/',
                'text' => 'About Waterproof Luxury Vinyl',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/waterproof-luxury-vinyl/products/',
                'text' => 'Waterproof Luxury Vinyl Products',
            );
        }
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'laminate_catalog' )) {
        if($instock[0] == "1"){
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/in-stock-laminate-products/',
                'text' => 'In Stock Laminate Products',
            );
        }else{
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/laminate/',
                'text' => 'About Laminate',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/laminate/products/',
                'text' => 'Laminate Products',
            );
        }    
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'tile_catalog' )) {
        if($instock[0] == "1"){
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/in-stock-tile-products/',
                'text' => ' In Stock Tile Products',
            );
        }else{
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/tile/',
                'text' => 'About Tile',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/tile/products/',
                'text' => 'Tile Products',
            );
        }    
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    return $links;
}

/*
    Gravity Forms => 1.9
*/

add_filter('gform_form_args', 'no_ajax_on_all_forms', 10, 1);
function no_ajax_on_all_forms($args){
    $args['ajax'] = false;
    return $args;
}

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js", array(), '3.2.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );